import platform
from beexporter.ui.ui_main import Ui_MainWindow
from beexporter.ui.ui_about import Ui_AboutDialog
from beexporter.version import version
from beexporter.workers import OpenWorker, ExportWorker
from PyQt5.QtWidgets import QDialog, QFileDialog, QMainWindow, QMessageBox
from os import getenv, path


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.setupUi(self)

        if platform.system() == "Windows":  # Set were the saves folder according to os
            self.saves_folder = path.join(getenv('APPDATA'), ".minecraft/saves/")
        elif platform.system() == "Darwin":
            self.saves_folder = path.expanduser("~/Library/Application Support/minecraft/saves/")
        else:  # Assume that any other os is linux like
            self.saves_folder = path.expanduser("~/.minecraft/saves/")

        self.last_folder = self.saves_folder
        self.open_thread = None
        self.export_thread = None
        self.world = None

    def error_msg(self, msg):
        QMessageBox.critical(self, "Error", msg)

    def export(self):
        name = self.optionNameLineEdit.text().replace("/", "").replace("\\", "").replace(":", "").replace("*", "")\
            .replace("?", "").replace("\"", "").replace("<", "").replace(">", "").replace("|", "")
        if name == "":
            QMessageBox.warning(self, "Error", "You have not entered a name")
        elif self.optionOutFileLineEdit.text() == "":
            QMessageBox.warning(self, "Error", "You have not entered a path")
        else:
            self.exportButton.setEnabled(False)
            if (self.export_thread is not None) and self.export_thread.isRunning():
                self.export_thread.terminate()
            self.export_thread = ExportWorker(self.world, name, name[:14], self.optionOutFileLineEdit.text(),
                                              self.optionExportBlocksCheckBox.isChecked(),
                                              self.optionExportAirCheckBox.isChecked(),
                                              self.optionMaxBlocksTickSpinBox.value())
            self.export_thread.done.connect(self.export_finished)
            self.export_thread.update_progress.connect(self.update_progress_bar)
            self.export_thread.error.connect(self.error_msg)
            self.export_thread.start()

    def export_finished(self):
        QMessageBox.information(self, "Success", "The selection has successfully been exported.")
        self.exportButton.setEnabled(True)

    def save_browser(self):
        directory = path.expanduser("~")
        if path.split(self.optionOutFileLineEdit.text())[0] != "":
            directory = path.split(self.optionOutFileLineEdit.text())[0]
        file_path = QFileDialog.getSaveFileName(self, "Save selection", directory,
                                                "Zip compressed file (*.zip);;All Files (*)")
        if file_path[0] != "":
            if file_path[1] == "Zip compressed file (*.zip)" and file_path[0][-4:] != ".zip":
                self.optionOutFileLineEdit.setText(file_path[0] + ".zip")
            else:
                self.optionOutFileLineEdit.setText(file_path[0])

    def open_about(self):
        dialog = QDialog(self)
        dialog.ui = Ui_AboutDialog()
        dialog.ui.setupUi(dialog)
        dialog.ui.version.setText(version)
        dialog.exec()

    def open_event(self):
        world_folder = QFileDialog.getExistingDirectory(self, "Select a world folder", self.last_folder)
        if world_folder != "":
            self.last_folder = world_folder
            if path.exists(path.join(world_folder, "level.dat")):
                if (self.open_thread is not None) and self.open_thread.isRunning():
                    self.open_thread.terminate()
                self.open_thread = OpenWorker(world_folder)
                self.open_thread.done.connect(self.set_world)
                self.open_thread.error.connect(self.error_msg)
                self.open_thread.start()
            else:
                QMessageBox.warning(self, "Error", "Folder does not contain level.dat is it a valid world?")

    def set_world(self):
        self.world = self.open_thread.world
        self.infoName.setText(self.world.name)
        self.infoSize.setText(str(1 + self.world.max_pos[0] - self.world.min_pos[0]) + " x " +
                              str(1 + self.world.max_pos[1] - self.world.min_pos[1]) + " x " +
                              str(1 + self.world.max_pos[2] - self.world.min_pos[2]))
        self.infoVolume.setText(str((1 + self.world.max_pos[0] - self.world.min_pos[0]) *
                                (1 + self.world.max_pos[1] - self.world.min_pos[1]) *
                                (1 + self.world.max_pos[2] - self.world.min_pos[2])))
        self.exportButton.setEnabled(True)

    def update_progress_bar(self, value):
        self.progressBar.setValue(value)
