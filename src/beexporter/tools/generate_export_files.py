from json import dumps
from os import path, walk

out = {}

for root, _, files in walk("datapacks/import_template/"):
    for file in files:
        f = open(path.join(root, file), "r")
        out[path.join(root, file)[26:]] = "".join(f.readlines())
        f.close()
f = open("src/beexporter/workers/export_files.py", "w")
f.write("# This file is auto generated any edits will be automatically removed\n")
f.write("files = ")
f.write(dumps(out))
f.write("\n")
f.close()
