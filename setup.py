from setuptools import setup, find_packages
from os.path import exists


version = {}

f = open("src/beexporter/version.py", "r")
exec(f.read(), version)
f.close()

setup(name='beexporter',
      version=version["version"],
      description="A program used to export from minecraft with bling edit",
      keywords='minecraft',
      author='ParkerMc',
      author_email='parkermc@parkermc.net',
      url='https://gitlab.com/ParkerMc/bling-edit-exporter',
      license='GNU GPLv3',
      packages=find_packages('src', include=["beexporter*"]),
      package_dir={'': 'src'},
      install_requires=["PyQt5", "nbt"],
      )
