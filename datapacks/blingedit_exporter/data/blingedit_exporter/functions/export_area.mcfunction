# Set the export area

scoreboard players operation Global be_export_xmin = Global box_xmin
scoreboard players operation Global be_export_ymin = Global box_ymin
scoreboard players operation Global be_export_zmin = Global box_zmin
scoreboard players operation Global be_export_xmax = Global box_xmax
scoreboard players operation Global be_export_ymax = Global box_ymax
scoreboard players operation Global be_export_zmax = Global box_zmax

# Send message with instructions on what to do next
tellraw @s ["",{"text":"Area marked for exporting to finish export close the world and use the program to export","color":"green"}]
