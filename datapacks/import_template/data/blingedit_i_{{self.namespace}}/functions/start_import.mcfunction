execute as @s run function blingedit:get_dest_minmax
scoreboard players add @e _age 1
summon armor_stand ~ ~ ~ {CustomNameVisible:0b,NoGravity:1b,Invulnerable:1b,Invisible:1b,NoBasePlate:0b,Tags:["import"],CustomName:"{\"text\":\"i_{{self.namespace}}\"}"}
scoreboard players add @e _age 1
scoreboard players set @e[scores={_age=1}] bee_age 0
execute as @e[scores={_age=1}] run function blingedit_i_{{self.namespace}}:move_armor_stand
bossbar set bee_progress players @s
function blingedit:cancel
