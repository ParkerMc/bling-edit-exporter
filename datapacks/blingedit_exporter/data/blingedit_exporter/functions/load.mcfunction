# Scoreboards for the plugin used to store pos to export
scoreboard objectives add be_export_xmin dummy
scoreboard objectives add be_export_ymin dummy
scoreboard objectives add be_export_zmin dummy
scoreboard objectives add be_export_xmax dummy
scoreboard objectives add be_export_ymax dummy
scoreboard objectives add be_export_zmax dummy

