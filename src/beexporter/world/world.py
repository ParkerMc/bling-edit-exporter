from nbt import nbt
import nbt.world as nbt_world
from os import path
from beexporter.world.chunk import Chunk


class World:
    def __init__(self, world_folder):
        self.world_folder = world_folder
        self.name = ""
        self.version = ""
        self.worldObj = None
        self.min_pos = (0, 0, 0)
        self.max_pos = (0, 0, 0)

    def load(self):
        self.worldObj = nbt_world.AnvilWorldFolder(self.world_folder)
        level_file = nbt.NBTFile(path.join(self.world_folder, "level.dat"), "rb")
        self.name = level_file["Data"]["LevelName"].value
        self.version = level_file["Data"]["Version"]["Name"].value

        if path.exists(path.join(self.world_folder, "data/scoreboard.dat")):
            min_pos = [0, 0, 0]
            min_pos_set = [False, False, False]
            max_pos = [0, 0, 0]
            max_pos_set = [False, False, False]
            scoreboard_file = nbt.NBTFile(path.join(self.world_folder, "data/scoreboard.dat"), "rb")
            for score in scoreboard_file["data"]["PlayerScores"]:
                if "be_export_" in score["Objective"].value:
                    if score["Objective"].value == "be_export_xmin":
                        min_pos[0] = int(score["Score"].value)
                        min_pos_set[0] = True
                    elif score["Objective"].value == "be_export_ymin":
                        min_pos[1] = score["Score"].value
                        min_pos_set[1] = True
                    elif score["Objective"].value == "be_export_zmin":
                        min_pos[2] = score["Score"].value
                        min_pos_set[2] = True
                    elif score["Objective"].value == "be_export_xmax":
                        max_pos[0] = score["Score"].value
                        max_pos_set[0] = True
                    elif score["Objective"].value == "be_export_ymax":
                        max_pos[1] = score["Score"].value
                        max_pos_set[1] = True
                    elif score["Objective"].value == "be_export_zmax":
                        max_pos[2] = score["Score"].value
                        max_pos_set[2] = True
            if not (min_pos_set[0] and min_pos_set[1] and min_pos_set[2] and
                    max_pos_set[0] and max_pos_set[1] and max_pos_set[2]):
                raise Exception("Not all of the scoreboard variables are set!")
            else:
                self.min_pos = (min_pos[0], min_pos[1], min_pos[2])
                self.max_pos = (max_pos[0], max_pos[1], max_pos[2])
        else:
            raise Exception("scoreboad.dat does not exist")

    def get_chunk(self, x, y, z):
        return Chunk(self, x, y, z)
